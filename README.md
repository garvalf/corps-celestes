# Corps Célestes


Musical sources for the "Corps Célestes" album by Garvalf, available on https://garvalf.bandcamp.com/


The final PDF version can be found in the "Releases" tab :  https://gitlab.com/garvalf/corps-celestes/-/releases



## Setup

- Get musescore on https://musescore.org/

You can use the AppImage, or something else. You might need to adapt the MUSESCORE variable into the compile.sh script.


- Install pdftk


On Ubuntu-based systems, you'll have to use snap:
 
```
sudo snap install pdftk
```

to remove the snap restrictions (can't use it on /tmp or removable devices), do:

```
sudo ln -s /snap/pdftk/current/usr/bin/pdftk /usr/bin/pdftk
```


## Generate the final partition and the midi files

Just run ``compile.sh``



## Additional informations

The scores had been processed with muse-sequencer and the final mix and mastering has been made in ardour.

The soundfonts used on this work are: 

- Sonatina Orchestra
- Titanic
- Timbre heaven
- Fluid R3
- SJO
- Crisis 1.8
- Merlin


And some synths:

- Dexed
- Zynaddsubfx



## Licence

Attribution-ShareAlike 4.0 International (**CC BY-SA** 4.0) 

You are free to:

- **Share** — copy and redistribute the material in any medium or format
- **Adapt** — remix, transform, and build upon the material for any purpose, even commercially. 
	
	
Under the following terms:

- **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

- **ShareAlike** — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original. 

