#!/bin/bash


MUSESCORE="/opt/MuseScore-3.6.2.548021370-x86_64.AppImage"



# generate midi and pdf

cd musescore

for a in *.mscz 
	do 
	     $MUSESCORE $a -o ../midi/${a%.*}.mid ; 
	     $MUSESCORE $a -o ../pdf/${a%.*}.pdf ; 
done ;
	
	
# generate final pdf

cd ..

pdftk pdf/*.pdf cat output /tmp/out.pdf
#pdftk /tmp/out.pdf dump_data output meta.txt
pdftk /tmp/out.pdf update_info pdf/bookmarks.txt output garvalf_-_corps_celestes.pdf


